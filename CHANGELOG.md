## [5.0.0] - 17.11.2021

* Global events moved to named parameter.
* `isClosed` flag.

## [4.1.0] - 11.03.2021

* Update rxdart dependency.

## [4.0.1] - 11.03.2021

* Null-safety fixes.

## [4.0.0] - 11.03.2021

* Update dependencies.
* Null-safety.

## [3.0.1] - 8.12.2020

* Update repository link.

## [3.0.0] - 6.08.2020

* Return previous state inside `onTransition`.
  
## [2.0.3] - 26.07.2020

* Use pedantic linter.

## [2.0.2] - 24.07.2020

* Fix concurrency bug.

## [2.0.1] - 23.07.2020

* Fix restart issue.

## [2.0.0] - 23.07.2020

* Drop bloc sub-dependency.

## [1.0.1] - 23.07.2020

* Update bloc dependency to 5.0.1.

## [1.0.0] - 8.05.2020

* Update bloc dependency to 4.0.0.

## [0.1.0+2] - 2.12.2019

* Wider dependencies.
  
## [0.1.0+1] - 1.12.2019

* Promote to 0.1.0.
  
## [0.0.8] - 1.12.2019

* Fix side effect call issue.

## [0.0.7] - 1.12.2019

* Fix dynamic cast bug.

## [0.0.6] - 1.12.2019

* Improved state and event cast.

## [0.0.5] - 28.11.2019

* Repair api doc sample.

## [0.0.4] - 28.11.2019

* Update description.

## [0.0.3] - 28.11.2019

* Add example.
  
## [0.0.2] - 28.11.2019

* Update readme.

## [0.0.1] - 23.11.2019

* Initial release.

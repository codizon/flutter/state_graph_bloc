import 'single_live_event.dart';

/// Subject for [SingleLiveEvent]s.
/// Exposes [Sink] (see [add]) interface and [stream].
/// For implementation see [StateGraphBloc] [singleLiveEventSubject] method.
abstract class SingleLiveEventSubject<T> implements Sink<T> {
  /// Returns [SingleLiveEvent] [Stream].
  Stream<SingleLiveEvent<T>> get stream;
}

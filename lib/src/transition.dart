// Definitions

/// {@template state_transition_entry}
/// Entry for [StateGraph], contains optional [transition] and [sideEffect].
/// {@endtemplate}
class StateTransitionEntry<State, Event> {
  /// State to new state conversion.
  final StateTransition<State, Event, State>? transition;

  /// Side effect of transition (e.g. logging).
  final SideEffect<State, Event>? sideEffect;

  /// {@macro state_transition_entry}
  StateTransitionEntry({this.transition, this.sideEffect});
}

/// Transition builder function.
typedef StateTransition<InState, Event, OutState> = OutState Function(
    InState state, Event event);

/// Side effect builder function.
typedef SideEffect<State, Event> = void Function(State state, Event event);

// Sugar functions

/// Makes [StateTransitionEntry] with just state-to-state [transition].
StateTransitionEntry<State, Event>
    transition<State, Event, InState extends State, RealEvent extends Event>(
  StateTransition<InState, RealEvent, State> transition,
) {
  return StateTransitionEntry<State, Event>(
      transition: (state, event) =>
          transition(state as InState, event as RealEvent));
}

/// Makes [StateTransitionEntry] with both [transition] and [sideEffect].
StateTransitionEntry<State, Event> transitionWithSideEffect<State, Event,
    InState extends State, RealEvent extends Event>(
  StateTransition<InState, RealEvent, State> transition,
  SideEffect<InState, RealEvent> sideEffect,
) {
  return StateTransitionEntry<State, Event>(
    transition: (state, event) =>
        transition(state as InState, event as RealEvent),
    sideEffect: (state, event) =>
        sideEffect(state as InState, event as RealEvent),
  );
}

/// Makes [StateTransitionEntry] with just [sideEffect] of transition.
StateTransitionEntry<State, Event>
    sideEffect<State, Event, InState extends State, RealEvent extends Event>(
  SideEffect<InState, RealEvent> sideEffect,
) {
  return StateTransitionEntry<State, Event>(
    sideEffect: (state, event) =>
        sideEffect(state as InState, event as RealEvent),
  );
}

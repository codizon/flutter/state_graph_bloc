/// {@template single_live_event}
/// Single live event.
/// {@endtemplate}
class SingleLiveEvent<T> {
  T? _value;

  /// {@macro single_live_event}
  SingleLiveEvent(this._value);

  /// One-time usage of event [value].
  void use(void Function(T value) action) {
    final currentValue = _value;
    _value = null;
    if (currentValue != null) action(currentValue);
  }
}

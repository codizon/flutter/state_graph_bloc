import 'package:state_graph_bloc/state_graph_bloc.dart';

/// {@template graph}
/// State graph.
///
/// Defined by [State]s and [Event]s. Makes transitions based on [graph] and (optionally) [globalEvents].
/// {@endtemplate}
class StateGraph<Event, State> {
  /// Map defines [State] to list of available [Event]s.
  /// Each entry refers to [transition].
  ///
  /// Example:
  /// ```dart
  /// var graph = StateGraph<Event, State>({
  ///   State1: {
  ///     GoTo2Event: transition((state, event) => State2()),
  ///     GoTo3Event: transition((state, event) => State3()),
  ///   },
  ///   State2: {
  ///     GoTo3Event: transitionWithSideEffect(
  ///        // Add parameter types to use state and event values.
  ///        (State2 state, GoTo3Event event) => State3(),
  ///        (State2 state, GoTo3Event event) => print("Transition made from State2!"),
  ///     ),
  ///   },
  ///   State3: {
  ///     GoTo3Event: noTransition(
  ///        (state, event) => print("Already in state3!"),
  ///     ),
  ///   },
  /// });
  /// ```
  final Map<Type, Map<Type, StateTransitionEntry<State, Event>>> graph;

  /// Optional map of global events which can be called in any [State].
  ///
  /// Map defines [State] to list of available [Event]s.
  /// Each entry refers to [transition], [transitionWithSideEffect] or [sideEffect].
  ///
  /// Example:
  /// ```dart
  /// var globalEvents = {
  ///   GoTo2Event: transition((state, event) => State2()),
  ///   GoTo3Event: sideEffect((state, event) => print("Hello!")),
  /// }
  /// ```
  final Map<Type, StateTransitionEntry<State, Event>> globalEvents;

  /// {@macro graph}
  StateGraph(this.graph, {this.globalEvents = const {}});

  /// Makes transition from [state] on [event] based on [graph].
  State call(State state, Event event) {
    final transitionEntry = findTransitionEntry(state, event);
    if (transitionEntry == null) return state;
    return transitionEntry.transition?.call(state, event) ?? state;
  }

  /// Calls side effect defined in [graph] for [state] and [event].
  void callSideEffect(State state, Event event) {
    final transitionEntry = findTransitionEntry(state, event);
    transitionEntry?.sideEffect?.call(state, event);
  }

  /// Finds [graph]'s [StateTransitionEntry] for [state] and [event].
  /// If there's no such entry, [globalEvents] lookup is made.
  /// If no entry is found, [null] is returned.
  StateTransitionEntry<State, Event>? findTransitionEntry(
    State state,
    Event event,
  ) {
    final stateEntry = graph[state.runtimeType];
    if (stateEntry != null) {
      final transitionEntry = stateEntry[event.runtimeType];
      if (transitionEntry != null) return transitionEntry;
    }

    final globalTransition = globalEvents[event.runtimeType];
    if (globalTransition != null) return globalTransition;

    return null;
  }
}

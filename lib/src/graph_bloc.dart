import 'dart:async';

import 'package:rxdart/rxdart.dart';

import '../state_graph_bloc.dart';
import 'graph.dart';

/// Base class for Blocs using [StateGraph], based on bloc package.
abstract class StateGraphBloc<Event, State> {
  /// An initial state.
  final State initialState;
  State _lastState;

  bool _isClosed = false;
  bool get isClosed => _isClosed;

  final Subject<State> _stateSubject;
  final Subject<Event> _eventSubject = PublishSubject();

  /// Current state stream.
  Stream<State> get state => _stateSubject;

  /// Last bloc state.
  State? get lastState => _lastState;

  final CompositeSubscription _compositeSubscription = CompositeSubscription();
  StreamSubscription? _loopSubscription;

  StateGraph<Event, State>? _graph;

  /// Creates a state graph bloc with a given [initialState].
  StateGraphBloc(this.initialState)
      : _lastState = initialState,
        _stateSubject = BehaviorSubject.seeded(initialState) {
    _runLoop();
  }

  /// Resets state machine to given [state].
  void reset(State state) {
    _loopSubscription?.cancel();
    _lastState = state;
    _stateSubject.add(state);
    _runLoop();
  }

  /// Adds an event to a queue.
  void add(Event event) {
    _eventSubject.add(event);
  }

  /// Method for building [StateGraph] instance.
  StateGraph<Event, State> buildGraph();

  /// Transforms events.
  Stream<Event> transformEvents(Stream<Event> events) {
    return events;
  }

  /// Method called on each state transition.
  void onTransition(State previousState, Event event, State newState) {}

  void _runLoop() {
    _loopSubscription = transformEvents(_eventSubject)
        .scan<State>((State? previousState, Event event, int _) {
          final entry = _mapEventToState(previousState!, event);
          if (entry == null) {
            return previousState;
          } else {
            final newState =
                entry.transition?.call(previousState, event) ?? previousState;
            _lastState = newState;
            entry.sideEffect?.call(previousState, event);
            onTransition(previousState, event, newState);
            return newState;
          }
        }, _lastState)
        .distinct()
        .listen(_stateSubject.add);
    _loopSubscription?.addTo(_compositeSubscription);
  }

  StateTransitionEntry<State, Event>? _mapEventToState(
      State state, Event event) {
    final graph = _getOrCreateGraph()!;
    return graph.findTransitionEntry(state, event);
  }

  /// Maps current state stream to the new one using [convert] method.
  /// Uses [distinct] operator to skip recurring values.
  Stream<N> bindState<N>(N Function(State currentState) convert) =>
      state.map(convert).distinct();

  StateGraph<Event, State>? _getOrCreateGraph() {
    if (_graph == null) {
      _graph = buildGraph();
      assert(_graph != null);
    }
    return _graph;
  }

  /// Launch [action] subscription that will be closed on Bloc's [close] method call.
  /// Converts [action] to [Stream] and returns [Future] which is exactly [launchStream] subscription.
  StreamSubscription<T> launchFuture<T>(
    Future<T> action, {
    void Function(T event)? onData,
    Function? onError,
    void Function()? onDone,
    bool? cancelOnError,
  }) {
    final stream = action.asStream();
    return launchStream(
      stream,
      onData: onData,
      onError: onError,
      onDone: onDone,
      cancelOnError: cancelOnError,
    );
  }

  /// Launch [action] subscription that will be cancelled on Bloc's [close] method call.
  /// Returns immediately cancelled [StreamSubscription] if bloc was already disposed.
  StreamSubscription<T> launchStream<T>(
    Stream<T> action, {
    void Function(T event)? onData,
    Function? onError,
    void Function()? onDone,
    bool? cancelOnError,
  }) {
    if (_compositeSubscription.isDisposed) {
      return _EmptyStreamSubscription<T>();
    }

    final subscription = action.listen(
      onData ?? (_) {},
      onError: onError,
      onDone: onDone,
      cancelOnError: cancelOnError,
    );

    try {
      return _compositeSubscription.add(subscription);
    } on String {
      // Composite subscription was disposed in the meantime.
      subscription.cancel();
      return _EmptyStreamSubscription<T>();
    }
  }

  /// Builds subject for [SingleLiveEvent]s.
  /// Is auto-closed on [close] method.
  SingleLiveEventSubject<T> singleLiveEventSubject<T>() {
    final result = _AutoCloseSingleLiveEventSubject<T>();
    _compositeSubscription.add(result);
    return result;
  }

  /// Closes bloc.
  void close() {
    _isClosed = true;
    _eventSubject.drain();
    _compositeSubscription.dispose();
  }
}

class _AutoCloseSingleLiveEventSubject<T> extends StreamSubscription<T>
    with _EmptyStreamSubscription<T>, SingleLiveEventSubject<T> {
  final _subject = BehaviorSubject<SingleLiveEvent<T>>();

  @override
  Stream<SingleLiveEvent<T>> get stream => _subject;

  @override
  void add(T data) {
    _subject.add(SingleLiveEvent(data));
  }

  @override
  void close() {
    _subject.close();
  }

  @override
  Future cancel() {
    super.cancel();
    close();
    return Future.value(null);
  }
}

class _EmptyStreamSubscription<T> implements StreamSubscription<T> {
  @override
  Future<E> asFuture<E>([E? futureValue]) {
    return Future.value(futureValue);
  }

  @override
  Future cancel() {
    return Future.value(null);
  }

  @override
  bool get isPaused => false;

  @override
  void onData(void Function(T data)? handleData) {}

  @override
  void onDone(void Function()? handleDone) {}

  @override
  void onError(Function? handleError) {}

  @override
  void pause([Future? resumeSignal]) {}

  @override
  void resume() {}
}

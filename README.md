# state_graph_bloc

State Graph Builder for Dart. Allows to define states, their transitions and side effects in a result of incoming events.

Inspired by [Tinder's state machine Kotlin DSL](https://github.com/Tinder/StateMachine) and [BLoC package](https://pub.dev/packages/bloc).

## Usage

Build a bloc and subscribe with `StreamBuilder` or `listen()`.

```dart
class DummyBloc extends StateGraphBloc<DummyEvent, DummyState> {
  DummyBloc() : super(StateInitialising());
  
  @override
  StateGraph<DummyEvent, DummyState> buildGraph() =>
      StateGraph<DummyEvent, DummyState>(
        {
          StateInitialising: {
            LoadEvent: transitionWithSideEffect(
              (state, event) => StateLoading(),
              (state, event) {
                // There could be some async loading. Use [launchFuture] or [launchStream]
                // to fire async processing and properly handle bloc closing.
                add(LoadingCompletedEvent());
              },
            ),
          },
          StateLoading: {
            LoadingCompletedEvent: transition((state, _) => StateReady(0)),
          },
          StateReady: {
            IncrementEvent: transition(
              (state, event) => StateReady((state as StateReady).counter + 1),
            ),
            // Test global event override.
            RestartEvent: transition((state, _) => StateReady(0)),
          },
        },
        globalEvents: {
          RestartEvent: transition((state, event) {
            return StateInitialising();
          })
        },
      );

  // Bind some variables based on [state].
  Stream<bool> isLoading() => bindState((state) {
        if (state is StateLoading) {
          return true;
        } else {
          return false;
        }
      });
}
```

## Samples

`test` directory contains all State Graph Bloc use cases.
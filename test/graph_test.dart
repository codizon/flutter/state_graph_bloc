import 'package:state_graph_bloc/state_graph_bloc.dart';
import 'package:test/test.dart';

import 'data/dummy_event.dart';
import 'data/dummy_state.dart';

void main() {
  final dummyGraph = StateGraph<DummyEvent, DummyState>(
    {
      StateInitialising: {
        LoadEvent: transition((dynamic state, dynamic event) => StateLoading()),
      },
      StateLoading: {
        LoadingCompletedEvent:
            transition((dynamic state, dynamic event) => StateReady(0)),
      },
      StateReady: {
        IncrementEvent: transition(
          (StateReady state, IncrementEvent event) =>
              StateReady(state.counter + event.count),
        ),
        // Test global event override.
        RestartEvent:
            transition((dynamic state, dynamic event) => StateReady(0)),
      },
    },
    globalEvents: {
      RestartEvent: transition((dynamic state, dynamic event) {
        return StateInitialising();
      }),
    },
  );

  group('GraphTest', () {
    test('should move between known states', () {
      expect(
        dummyGraph(StateInitialising(), LoadEvent()),
        StateLoading(),
      );

      expect(
        dummyGraph(StateLoading(), LoadingCompletedEvent()),
        StateReady(0),
      );

      expect(
        dummyGraph(StateReady(1), IncrementEvent(2)),
        StateReady(3),
      );
    });

    test('should not move on unknown event', () {
      expect(
        dummyGraph(StateReady(1), LoadingCompletedEvent()),
        StateReady(1),
      );
    });

    test('should resolve global event transition', () {
      // overriden event
      expect(
        dummyGraph(StateReady(1), RestartEvent()),
        StateReady(0),
      );

      // non-overriden
      expect(
        dummyGraph(StateLoading(), RestartEvent()),
        StateInitialising(),
      );
    });

    test('should find global event transition', () {
      expect(
        dummyGraph.findTransitionEntry(StateInitialising(), RestartEvent()),
        isNotNull,
      );
      expect(
        dummyGraph.findTransitionEntry(StateLoading(), RestartEvent()),
        isNotNull,
      );
      expect(
        dummyGraph.findTransitionEntry(StateReady(0), RestartEvent()),
        isNotNull,
      );
    });
  });
}

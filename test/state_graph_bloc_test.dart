import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:test/test.dart';

import 'data/dummy_bloc.dart';
import 'data/dummy_event.dart';
import 'data/dummy_state.dart';

void main() {
  late DummyBloc bloc;
  late MockedCallback onTransitionCallback;

  setUp(() {
    onTransitionCallback = MockedCallback();
    bloc = DummyBloc(
      onTransitionCallback: (previousState, event, newState) =>
          onTransitionCallback(previousState, event, newState),
    );
  });

  tearDown(() async {
    bloc.close();
  });

  group('StateGraphBlocTest', () {
    test('should emit proper states', () async {
      // When
      bloc.add(LoadEvent());

      // Then
      await expectLater(
        bloc.state,
        emitsInOrder([
          StateInitialising(),
          StateLoading(),
          StateReady(0),
        ]),
      );
    });

    test('should emit proper transformed data', () async {
      // Given
      final loading = bloc.isLoading();

      // When
      bloc.add(LoadEvent());
      bloc.add(LoadingCompletedEvent());

      // Then
      await expectLater(
        loading,
        emitsInOrder([
          false, // StateInitialising
          true, // StateLoading
          false, // StateReady
        ]),
      );
    });

    test('should use global events', () async {
      // When
      bloc.add(LoadEvent());

      // Then
      await expectLater(
          bloc.state,
          emitsInOrder([
            StateInitialising(),
            StateLoading(),
            StateReady(0),
          ]));

      // And
      bloc.add(IncrementEvent(1));
      bloc.add(RestartEvent());

      // Then
      await expectLater(
          bloc.state,
          emitsInOrder([
            StateReady(0),
            StateReady(1),
            StateReady(0),
          ]));
    });

    test('should trigger onTransition callback', () async {
      // When
      bloc.add(LoadEvent());

      await expectLater(
          bloc.state,
          emitsInOrder([
            StateInitialising(),
            StateLoading(),
            StateReady(0),
          ]));

      bloc.add(IncrementEvent(1));
      bloc.add(RestartEvent());

      await expectLater(
          bloc.state,
          emitsInOrder([
            StateReady(0),
            StateReady(1),
            StateReady(0),
          ]));

      // Then
      expect(onTransitionCallback.calls, [
        CallbackTriple(StateInitialising(), LoadEvent(), StateLoading()),
        CallbackTriple(StateLoading(), LoadingCompletedEvent(), StateReady(0)),
        CallbackTriple(StateReady(0), IncrementEvent(1), StateReady(1)),
        CallbackTriple(StateReady(1), RestartEvent(), StateReady(0)),
      ]);
    });

    test('should launch future', () async {
      // Given
      final future = Future.value('a');

      // When
      String? receivedData;
      await bloc.launchFuture(future, onData: (dynamic data) {
        receivedData = data;
      });

      // Then
      expect(receivedData, 'a');
    });

    test('should close future launched after bloc was closed', () async {
      // Given
      final future = Future.value('a');

      // When
      bloc.close();
      String? receivedData;
      final subscription = bloc.launchFuture(future, onData: (dynamic data) {
        receivedData = data;
      });
      await subscription.asFuture();

      // Then
      expect(receivedData, null);
    });

    test('should set close flag', () async {
      expect(bloc.isClosed, false);

      // When
      bloc.close();

      // Then
      expect(bloc.isClosed, true);
    });

    test('should launch stream subscription', () async {
      // Given
      final stream = Stream.value('a');

      // When
      String? receivedData;
      final subscription = bloc.launchStream(stream, onData: (dynamic data) {
        receivedData = data;
      });
      await subscription.asFuture();

      // Then
      expect(receivedData, 'a');
    });

    test('should dispose stream subscription after bloc was closed', () async {
      // Given
      final stream = Stream.value('a');

      // When
      bloc.close();
      String? receivedData;
      final subscription = bloc.launchStream(stream, onData: (dynamic data) {
        receivedData = data;
      });
      await subscription.asFuture(null);

      // Then
      expect(receivedData, null);
    });

    test('should reset state', () async {
      // When
      bloc.reset(StateReady(7));

      // Then
      await expectLater(
        bloc.state,
        emitsInOrder([
          StateReady(7),
        ]),
      );
    });

    test('should distinct states', () async {
      // When
      bloc.add(RestartEvent());
      bloc.add(RestartEvent());
      bloc.add(RestartEvent());

      // Then
      await expectLater(
        bloc.state,
        emitsInOrder([
          StateInitialising(),
        ]),
      );
    });
  });
}

abstract class OnTransitionCallback {
  void call(DummyState previousState, DummyEvent event, DummyState newState);
}

class MockedCallback implements OnTransitionCallback {
  List<CallbackTriple> calls = [];

  @override
  void call(DummyState previousState, DummyEvent event, DummyState newState) {
    calls.add(CallbackTriple(previousState, event, newState));
  }
}

class CallbackTriple with EquatableMixin {
  final DummyState previousState;
  final DummyEvent event;
  final DummyState newState;

  CallbackTriple(this.previousState, this.event, this.newState);

  @override
  List<Object?> get props => [previousState, event, newState];
}

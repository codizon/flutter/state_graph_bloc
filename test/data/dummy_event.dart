import 'package:equatable/equatable.dart';

abstract class DummyEvent {}

class LoadEvent extends DummyEvent with EquatableMixin {
  @override
  List<Object> get props => [];
}

class LoadingCompletedEvent extends DummyEvent with EquatableMixin {
  @override
  List<Object> get props => [];
}

class LoadingErrorEvent extends DummyEvent with EquatableMixin {
  @override
  List<Object> get props => [];
}

class IncrementEvent extends DummyEvent with EquatableMixin {
  final int count;

  IncrementEvent(this.count);

  @override
  List<Object> get props => [count];
}

class SideEffectCauseEvent extends DummyEvent with EquatableMixin {
  @override
  List<Object> get props => [];
}

class RestartEvent extends DummyEvent with EquatableMixin {
  @override
  List<Object> get props => [];
}

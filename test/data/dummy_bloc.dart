import 'package:state_graph_bloc/state_graph_bloc.dart';

import 'dummy_event.dart';
import 'dummy_state.dart';

class DummyBloc extends StateGraphBloc<DummyEvent, DummyState> {
  final Function(
          DummyState previousState, DummyEvent event, DummyState newState)?
      onTransitionCallback;

  DummyBloc({this.onTransitionCallback}) : super(StateInitialising());

  @override
  void onTransition(
      DummyState previousState, DummyEvent event, DummyState newState) {
    super.onTransition(previousState, event, newState);
    onTransitionCallback?.call(previousState, event, newState);
  }

  @override
  StateGraph<DummyEvent, DummyState> buildGraph() =>
      StateGraph<DummyEvent, DummyState>(
        {
          StateInitialising: {
            LoadEvent: transitionWithSideEffect(
              (StateInitialising state, LoadEvent event) => StateLoading(),
              (StateInitialising state, LoadEvent event) {
                // There should be some fetching
                add(LoadingCompletedEvent());
              },
            ),
          },
          StateLoading: {
            LoadingCompletedEvent: transition(
                (StateLoading state, LoadingCompletedEvent event) =>
                    StateReady(0)),
          },
          StateReady: {
            IncrementEvent: transition(
              (StateReady state, IncrementEvent event) =>
                  StateReady(state.counter + 1),
            ),
            // Test global event override.
            RestartEvent: transition(
                (StateReady state, RestartEvent event) => StateReady(0)),
          },
        },
        globalEvents: {
          RestartEvent: transition((dynamic state, RestartEvent event) {
            return StateInitialising();
          })
        },
      );

  Stream<bool> isLoading() => bindState((state) {
        if (state is StateLoading) {
          return true;
        } else {
          return false;
        }
      });
}

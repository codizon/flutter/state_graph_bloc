import 'package:equatable/equatable.dart';

/// Dummy state, used for testing purposes.
/// We might use library for equality, but we don't want yet another library dependency.
abstract class DummyState {}

class StateInitialising extends DummyState with EquatableMixin {
  @override
  List<Object> get props => [];
}

class StateLoading extends DummyState with EquatableMixin {
  @override
  List<Object> get props => [];
}

class StateReady extends DummyState with EquatableMixin {
  StateReady(this.counter);

  final int counter;

  @override
  List<Object> get props => [counter];
}
